from tqdm import tqdm_notebook as tqdm
import re
from tqdm import tqdm_notebook as tqdm
from unicodedata import normalize
from keras.preprocessing import text
from nltk.tokenize import word_tokenize
import multiprocessing
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from keras.preprocessing import text
import nltk
from nltk import sent_tokenize
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.base import BaseEstimator, TransformerMixin
import pandas as pd
import json

def get_dataset(dataset_file,as_json=True):
    """Get dataset in pandas format or in json format.

    Keyword arguments:
    dataset_file -- path of csv
    as_json -- boolean (default True)

    """
    df = pd.read_csv(dataset_file)
    df.dropna(inplace=True)
    if as_json:
        return json.loads(df.to_json(orient='records'))
    return df

def clean_text(sentence, with_split=True):
    """Clean a sentence and then split it.

    Keyword arguments:
    sentence -- text string
    with_split -- split clean text (default True)

    """
    clean_sentence = text.text_to_word_sequence(sentence, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~\t\n0123456789“”', lower=True)
    clean_sentence =  normalize('NFKD', " ".join(clean_sentence)).encode('utf-8').decode('utf-8')
    clean_sentence = re.sub(r'\\u\w*', '', clean_sentence, flags=re.I)
    clean_sentence = clean_sentence.replace("'"," ")
    if with_split:
        return clean_sentence.split(' ')
    return clean_sentence

def get_clean_documents(data_frame, column,with_split=True):
    """Clean text in json array.

    Keyword arguments:
    data_frame -- json array
    column -- key of json to clean 
    with_split -- split clean text (default True)

    """
    return list(map(lambda x: clean_text(x[column],with_split), tqdm(data_frame)))


def get_clean_sentences(sentences, column,with_split=True):
    """Clean a sentences array.

    Keyword arguments:
    sentences -- sentence array
    with_split -- split clean text (default True)

    """
    return list(map(lambda x: clean_text(x,with_split), tqdm(sentences)))    
    
def get_tagged_data(data):
    """Return tagged data.

    Keyword arguments:
    data -- documents array

    """
    tagged_data = [TaggedDocument(words=doc, tags=[str(index)]) for index, doc in enumerate(data)]
    return tagged_data

def train_doc2vec(data,dm=0, dbow_words=1,workers=1,vector_size=300):
    """Return tagged data.

    Keyword arguments:
    data -- documents array

    """
    model = Doc2Vec(dm=dm, dbow_words=dbow_words, workers=workers,vector_size=vector_size)
    tagged_train_data = get_tagged_data(data)
    model.build_vocab(tagged_train_data)
    model.train(tagged_train_data, total_examples=model.corpus_count, epochs=model.epochs)
    return model

def save_model(model,name):
    """Save model.

    Keyword arguments:
    model -- doc2vec model
    name -- name of the model

    """
    model.save(name)

def load_model(name):
    """Return model.

    Keyword arguments:
    name -- name of the model

    """
    return Doc2Vec.load(name)

def get_sentence_corpus(corpus):
    """Return tokenized array sentence from a corpus.

    Keyword arguments:
    corpus -- documents array

    """
    sentences_from_corpus = []
    for text in corpus:
        sentences_from_corpus += sent_tokenize(text)
    return sentences_from_corpus

def get_sentence(text):
    """Tokenize single text.

    Keyword arguments:
    text -- string text

    """
    return sent_tokenize(text)

def get_stopwords():
    """Return default stopwords.

    """
    return set(stopwords.words('english'))

def tokenizer(str_input):
    """Return tokenized string.

    Keyword arguments:
    str_input -- string text

    """
    words = re.sub(r"[^A-Za-z0-9\-]", " ", str_input).lower().split()
    porter_stemmer=nltk.PorterStemmer()
    words = [porter_stemmer.stem(word) for word in words]
    return words

def get_tfidf(stop_words,with_stem=False):
    """Return default TfIdf data.

    Keyword arguments:
    stop_words -- stop words array
    with_stem -- include stemming process (default False)

    """
    if(with_stem):
        return TfidfVectorizer(stop_words=stop_words, min_df=10, max_df=0.5, 
                        ngram_range=(1,1),tokenizer=tokenizer)
    else:
        return TfidfVectorizer(stop_words=stop_words, min_df=10, max_df=0.5, 
                        ngram_range=(1,1),token_pattern='[a-z][a-z]+')
    
def get_tfidf_vectors(texts,stop_words):
    """Return tfidf vectors.

    Keyword arguments:
    texts -- documents array
    stop_words -- stop words array

    """
    tfidf = get_tfidf(stop_words=stop_words)
    return tfidf.fit_transform(texts), tfidf


class TextSelector(BaseEstimator, TransformerMixin):
    def __init__(self, field):
        self.field = field
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.field]

class NumberSelector(BaseEstimator, TransformerMixin):
    def __init__(self, field):
        self.field = field
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[[self.field]]