import seaborn as sns
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from keras.utils import np_utils
import h5py
from tqdm import tqdm
from collections import Counter
from sklearn.metrics import accuracy_score, precision_score, classification_report, confusion_matrix

def get_train_test(df,x_col='text',y_col='stars',test_size=0.3, balance=False):
    """Retuen train test with balance option.

    Keyword arguments:
    df -- pandas dataframe
    x_col -- columns to get x_train (default 'text')
    y_col -- target column (default 'stars')
    test_size -- split size (default 0.3)
    balance -- the imaginary part (default False)

    """
    X = df[x_col].values
    y = df[y_col].values
    if(balance):
        X, y = balance_classes(X, y)
    X_train, X_test, y_train, y_test = train_test_split(X, y,stratify=y,   test_size=test_size,random_state=42,shuffle=True)
    del X, y
    return X_train, X_test, y_train, y_test

#Todo make it with shuffle
def balance_classes(xs, ys):
    """Undersample xs, ys to balance classes.

    Keyword arguments:
    xs -- features
    ys -- target

    """

    freqs = Counter(ys)

    # the least common class is the maximum number we want for all classes
    max_allowable = freqs.most_common()[-1][1]
    num_added = {clss: 0 for clss in freqs.keys()}
    new_ys = []
    new_xs = []
    for i, y in enumerate(ys):
        if num_added[y] < max_allowable:
            new_ys.append(y)
            new_xs.append(xs[i])
            num_added[y] += 1
    return new_xs, new_ys

def dist_plot(series):
    """Plot distribution of series.

    Keyword arguments:
    series -- series to plot

    """
    sns.distplot(series)

def pad_matrix_array(matrixs):
    """Pad an matrix array, calling pad_matrix method.

    Keyword arguments:
    matrixs -- array of matrix

    """
    return np.array(list(map(pad_matrix , matrixs)))

def pad_matrix(matrix):
    """Pad a single matrix .

    Keyword arguments:
    matrix -- numpy ndarray

    """
    matrix = np.array(matrix)
    if(matrix.shape[0] >10):
        return matrix[:10,:]
    else:
        return np.pad(matrix, ((0,10-matrix.shape[0]),(0,0)),mode='constant')

def one_hot(vectors):
    """Convert vectors in one hot representation.

    Keyword arguments:
    vectors -- array of categorical targets

    """
    encoder = LabelEncoder()
    encoder.fit(vectors)
    # convert integers to dummy variables (i.e. one hot encoded)
    return np_utils.to_categorical(encoder.transform(vectors))
    
def save_chunks(h5_file_name, x,y,carrie=0):
    """Save data in H5 file.

    Keyword arguments:
    h5_file_name -- array of categorical targets
    x -- ndarray with features
    y -- array of targets

    """
    h5f = h5py.File(h5_file_name, 'w')
    X_dset = h5f.create_dataset('x', x.shape, dtype='f')
    X_dset[:] = x
    y_dset = h5f.create_dataset('y', y.shape, dtype='i')
    y_dset[:] = y
    h5f.close()
    
def print_report(y_test,preds):
    """Print a report with Accuracy and clasification report.

    Keyword arguments:
    y_test -- original target
    preds -- predicted target

    """
    print("Accuracy:", accuracy_score(y_test, preds))
    print(classification_report(y_test, preds))
    print(confusion_matrix(y_test, preds))
    
def print_keras_report(y_pred,y_true,classes):
    """Print a report with Accuracy and clasification report for keras output.

    Keyword arguments:
    y_test -- original target
    preds -- predicted target

    """
    def auxLambda(single_pred):
        maximo = np.max(single_pred)
        for i,x in enumerate(single_pred):
            if(maximo == x):
                return classes[i]
    y_pred = np.array(list(map(auxLambda,y_pred)))
    y_true = np.array(list(map(auxLambda,y_true)))
    accuracy = accuracy_score(y_true, y_pred)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))
    print("--------------Report-----------")
    print(classification_report(y_true, y_pred))