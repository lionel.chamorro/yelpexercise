# Yelp Rating Prediction

In this project is the development made to predict the rating with respect to comments.
It is distributed in several notebooks, in the section "Notebooks" the order for its execution is detailed.

*Requirements*

* fasttext 0.8.22  
* tensorflow-gpu 1.11.0 
* Keras 2.2.4
* tensorboard 0.4.0
* gensim 3.6.0
* xgboost 0.80
* h5py 2.8.0
* tqdm
* Keras-tqdm 2.0.1
* langdetect 1.0.7

*Notebooks*

The order of execution for the notebooks is as follows:

* DataExploration
* FeatureExtraction
* ProcessFastText
* ProcessDoc2Vec
* MLModels
* DeepModels

The notebooks are supported by 3 files where auxiliary functions were developed that I preferred to remove from the notebook for reuse and also for an aesthetic theme of the notebooks.

In DataExploration you can find an analysis of the dataset that was taken to perform the "Text + Rating" training.
After the analysis, a new clean dataset was generated for the following notebooks.

In FeatureExtraction, two doc2vec models are trained. The first one taking all the comment as vectors and the second one generating vectors for each sentence.

The processing notebooks are responsible for generating the training and validation dataset using the previously trained fasttext and doc2vec models.
For each model, two types of data are generated to train, one with a single vector per comment and another with a matrix composed with the vectors of the sentences of the comment.

Finally in the MLModels and DeepModels notebooks different architectures are trained. 
In MLModels the TfIdf algorithm is used to train the models. 
In DeepModels, training is carried out with the 4 types of data that were generated in the previous notebooks.

