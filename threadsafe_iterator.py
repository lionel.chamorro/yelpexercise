import threading

from keras.applications.inception_v3 import InceptionV3
from keras.optimizers import Adam
from keras.utils.io_utils import HDF5Matrix

#threadsafe_iter
class ThreadSafeIterator:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `next` method of given iterator/generator.
    """

    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return self.it.__next__()


def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """

    def g(*a, **kw):
        return ThreadSafeIterator(f(*a, **kw))

    return g


@threadsafe_generator       
def generator(hdf5_file, batch_size):
    """Yield batch training data.

    Keyword arguments:
    hdf5_file -- path of h5file
    batch_size -- number of batch size

    """
    x = HDF5Matrix(hdf5_file, 'x')
    y = HDF5Matrix(hdf5_file, 'y')
    while True:
        start, end = 0, batch_size
        while end < len(y):
            s = slice(start, end)
            yield x[s], y[s]
            start, end = end, end + batch_size


def data_statistic(train_dataset, test_dataset):
    """Return information about the train and test dataset.

    Keyword arguments:
    train_dataset -- h5 path of train_dataset 
    test_dataset -- h5 path of test_dataset

    """
    train_x = HDF5Matrix(train_dataset, 'x')
    test_x = HDF5Matrix(test_dataset, 'x')
    return train_x.end, test_x.end